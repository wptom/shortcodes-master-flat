<?php
/*
  Plugin Name: Shortcodes Master - Flat
  Plugin URI: https://lizatom.com/wordpress-flat-design/
  Version: 1.0.0
  Author: Lizatom.com
  Author URI: https://lizatom.com
  Description: Flat elements addon for Shortcodes Master.
  Text Domain: smmet
  Domain Path: /lang
 */

define( 'SMFLAT_PLUGIN_FILE', __FILE__ );
define( 'SMFLAT_PLUGIN_VERSION', '1.0.0' );

require_once 'inc/flat.php';
require_once 'inc/shortcodes.php';
