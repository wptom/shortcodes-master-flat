<?php
class Shortcodes_Master_Flat_Shortcodes
{

	static $globflattab = array();
	static $tab_count = 0;

	function __construct() { }
	/**
	 * flattabs
	 */
		public static function flattabs($atts = null, $content = null)	{
			$tabs_classes = '';
			$return = '';
			
			sm_query_asset( 'css', 'jquery-ui' );
			sm_query_asset( 'js', 'jquery' );
			sm_query_asset( 'js', 'jquery-ui-tabs' );
			sm_query_asset( 'js', 'sm-flat' );

			$atts=shortcode_atts(array
				(
					'id' => 'unique-id',
					'navigation' => 'top-left',
					'bg_color_default' => '#2B5797',
					'bg_color_current' => '#EFF4FF',
					'text_color_default' => '#ffffff',
					'text_color_current' => '#1D1D1D',
				), $atts, 'flattabs');           

			do_shortcode($content);

			if ($atts['navigation'] == 'bottom-left')
			{
				$tabs_classes='tabs-bottom';
			}

			if ($atts['navigation'] == 'left')
			{
				$tabs_classes='ui-tabs-vertical ui-helper-clearfix';
			}

			if (is_array(self::$globflattab))
			{
				$int='1';

				foreach (self::$globflattab as $tab)
				{

					$tabs[]='<li><a href="#' . $atts['id'] . '-' . $int . '">' . $tab['title'] . '</a></li>';
					$panes[]='' . '<div id="' . $atts['id'] . '-' . $int . '"><div class="sm-tabs-wrap-content">' . $tab['content'] . '</div></div>';
					$int++;
				}
				$return.='<div class="shortmas-flat-tabs ' . $tabs_classes
					. '" id="' . $atts['id'] . '" data-flat-tab-default-bg="' . $atts['bg_color_default'] . '" data-flat-tab-default-text="'
					. $atts['text_color_default'] . '" data-flat-tab-current-bg="' . $atts['bg_color_current']
					. '" data-flat-tab-current-text="' . $atts['text_color_current'] . '">';

				if ($atts['navigation'] != 'bottom-left')
				{
					$return.='<ul>';
					$return.='' . implode($tabs) . '';
					$return.='</ul>';
				}
				$return.='' . implode($panes) . '';

				if ($atts['navigation'] == 'bottom-left')
				{
					$return.='<ul>';
					$return.='' . implode($tabs) . '';
					$return.='</ul>';
				}
				$return.='</div>';

			}     

			self::$globflattab = array();
			self::$tab_count=0;            

			return $return;
		}
	/**
	 * flattab
	 */
	public static function flattab($atts = null, $content = null)
	{
		$atts=shortcode_atts(array('title' => 'Title %d'), $atts, 'flattab');

		$x=self::$tab_count;
		self::$globflattab[$x]=array
			(
				'title' => $atts['title'],
				'content' => $content
			);

		self::$tab_count++;
	}

	/**
	 * flataccordions
	 */
	public static function flataccordions($atts = null, $content = null)
	{
		$atts=shortcode_atts(array
			(
				'id'           => 'unique-id',
				'bg_header'    => '#2B5797',
				'text_header'  => '#EFF4FF',
				'bg_content'   => '#ffffff',    
				'text_content' => '#1D1D1D',
			), $atts, 'flataccordions');           
		
		sm_query_asset( 'css', 'jquery-ui' );
		sm_query_asset( 'js', 'jquery' );
		sm_query_asset( 'js', 'jquery-ui-accordion' );
		sm_query_asset( 'js', 'sm-flat' );

		$return = '<div id="'.$atts['id'].'" class="shortmas-flat-accordion" data-flat-acc-header-bg="'.$atts['bg_header'].'" data-flat-acc-header-text="'.$atts['text_header'].'" data-flat-acc-content-bg="'.$atts['bg_content'].'" data-flat-acc-content-text="'.$atts['text_content'].'">';
		$return .= do_shortcode($content);
		$return .= '</div>';

		return $return;
	}

	/**
	 * flataccordion
	 */
	public static function flataccordion($atts = null, $content = null)
	{
		$atts=shortcode_atts(array
			(
				'title' => 'Accordion %d'
			), $atts, 'flataccordion');

		$return = '<div>'.$atts['title'].'</div>';
		$return .= '<div>';
		$return .= $content;
		$return .= '</div>';

		return $return;
	}        

	/**
	 * flatinfobox
	 */
	public static function flatinfobox($atts = null, $content = null)
	{
		$atts=shortcode_atts(array
			(
				'id'           => 'unique-id-of-the-box',
				'class'        => '',
				'title'        => 'Title',
				'type'         => 'box',
				'icon'         => 'icon: heart',
				'bgcolor'      => '#1E7145',
				'text_color'   => '#ffffff',
				'icon_color'   => '#ffffff',
				'close_button' => 'yes'
			), $atts, 'flatinfobox');

		sm_query_asset( 'css', 'jquery-ui' );
		sm_query_asset( 'css', 'font-awesome' );
		sm_query_asset( 'js', 'jquery' );            
		sm_query_asset( 'js', 'sm-flat' );

		// icon
		if ( $atts['icon'] ) {
			if ( strpos( $atts['icon'], 'icon:' ) !== false ) {
				$icon = '<span class="shortmas-flat-box-icon fa fa-' . trim( str_replace( 'icon:', '', $atts['icon'] ) ) . '" style="color: ' . $atts['icon_color'] . '"></span>';
			}
			else $icon = '<img src="' . $atts['icon'] . '" alt="' . esc_attr( $content ) . '" />';
		}
		else {  $icon = ''; $style = 'style="display: block;"'; }

		// title
		if ( $atts['title'] ) { $title = '<span class="shortmas-flat-box-title" style="color: ' . $atts['text_color'] . ';">' . $atts['title'] . '</span>'; } else { $title = ''; }
		$return = '<style>#' . $atts['id'] . '.shortmas-flat-bubble:before {border-color: transparent ' . $atts['bgcolor'] . ' !important;}</style>';

		// close button
		if ( $atts['close_button'] == 'yes' ) { $close = '<div class="shortmas-close-box icon-remove"></div>'; } else { $close = ''; }     

		$return .= '<div  id="' . $atts['id'] . '" class="shortmas-flat-' . $atts['type'] . ' ' . $atts['class'] . '" style="background: ' . $atts['bgcolor'] . '; color: ' . $atts['text_color'] . ';">' . $icon . '<p>' . $title . '' . do_shortcode($content) . '</p>'.$close.'</div>';


		// Return shortcode string
		return $return;
	}

	/**
	 * flatdialog
	 */
	public static function flatdialog($atts = null, $content = null)
	{
		$atts=shortcode_atts(array
			(
				'id'                 => 'dialog-window-id',
				'title'              => '',
				'opener_id'          => 'window-opener-id',
				'titlebar_bgcolor'   => '#DA532C',
				'titlebar_textcolor' => '#ffffff',
				'content_bgcolor'    => '#E3A21A',
				'content_textcolor'  => '#ffffff',
				'showeffect'         => 'blind',
				'hideeffect'         => 'explode',
				'draggable'          => 'no'
			), $atts, 'flatdialog');

		sm_query_asset( 'css', 'jquery-ui' );

		sm_query_asset( 'js', 'jquery' );
		sm_query_asset( 'js', 'jquery-ui-core' );
		sm_query_asset( 'js', 'jquery-effects-core' );
		sm_query_asset( 'js', 'jquery-effects-'.$atts['showeffect'].'' );
		sm_query_asset( 'js', 'jquery-effects-'.$atts['hideeffect'].'' );
		sm_query_asset( 'js', 'jquery-ui-dialog' );

		sm_query_asset( 'js', 'sm-flat' );

		if (($atts['draggable'])=='yes') { $draggable = 'true'; } else { $draggable = 'false'; }

		$return = '<script type="text/javascript">
			jQuery(document).ready(function() {
				jQuery( "#'.$atts['id'].'" ).dialog({
					autoOpen: false,
						draggable: false,
						dialogClass: "shortmas-flat-dialog",
						create: function( event, ui ) {
							jQuery(".ui-dialog-titlebar .ui-icon-closethick, .ui-dialog-titlebar .ui-button-text").remove();
							var titlebarBgcolor = jQuery(this).attr("data-dialog-titlebar-bgcolor");
							var titlebarColor = jQuery(this).attr("data-dialog-titlebar-color");
							var contentBgcolor = jQuery(this).attr("data-dialog-content-bgcolor");
							var contentColor = jQuery(this).attr("data-dialog-content-color");
							var showeffect = jQuery(this).attr("data-dialog-show-effect");
							var hideeffect = jQuery(this).attr("data-dialog-hide-effect");
							jQuery(this).prev().css({"background-color" : titlebarBgcolor});
							jQuery(this).prev().css({"color" :titlebarColor});
							jQuery(this).css({"background-color" :contentBgcolor});
							jQuery(this).css({"color" :contentColor});
	},
		show: {
			effect: "'.$atts['showeffect'].'",
				duration: 300
	},
	hide: {
		effect: "'.$atts['hideeffect'].'",
			duration: 300
	}
	});
	jQuery( "#'.$atts['opener_id'].'" ).click(function(e) {
		jQuery( "#'.$atts['id'].'" ).dialog( "open" );
		e.preventDefault();
	});
	});</script>';
	$return .= '<div id="'.$atts['id'].'" title="'.$atts['title'].'" data-dialog-opener-id="'.$atts['opener_id'].'" data-dialog-titlebar-bgcolor="'.$atts['titlebar_bgcolor'].'" data-dialog-titlebar-color="'.$atts['titlebar_textcolor'].'" data-dialog-content-bgcolor="'.$atts['content_bgcolor'].'" data-dialog-content-color="'.$atts['content_textcolor'].'" data-dialog-show-effect="'.$atts['showeffect'].'" data-dialog-hide-effect="'.$atts['hideeffect'].'">';
	$return .= do_shortcode($content);
	$return .= '</div>';

	// Return shortcode string
	return $return;
	}  


	/**
	 * flatframe
	 */
	public static function flatframe($atts = null, $content = null)
	{
		$atts=shortcode_atts(array
			(
				'id' => 'flat-frame-id',
				'image' => '',
				'frame_color' => '#4A89DC',
				'icon' => 'icon: search-plus',
				'caption' => '' 
			), $atts, 'flatframe');
		sm_query_asset( 'css', 'font-awesome' );             
		sm_query_asset( 'js', 'jquery' );
		sm_query_asset( 'js', 'lightboxme' );              
		sm_query_asset( 'js', 'sm-flat' );

		// icon
		if ( $atts['icon'] ) {
			if ( strpos( $atts['icon'], 'icon:' ) !== false ) {
				$icon = trim( str_replace( 'icon:', '', $atts['icon'] ) );
			}
			else { $icon = '<img src="' . $atts['icon'] . '" alt="' . esc_attr( $content ) . '" />'; }
		}
		else {  $icon = ''; }
		// lightbox content
		if ( $content ) { 
			$lb =   $content; 
		} else { $lb = '<img src="'.$atts['image'].'" alt="image" class="shortmas-flat-box-image" />'; }

		$return = '<div class="shortmas-flat-image-frame shortmas-is-lightbox" style="background-color: '.$atts['frame_color'].';"><img src="'.$atts['image'].'" alt="image" class="shortmas-flat-box-image"  />
			<div class="shortmas-flat-overlay" id="'.$atts['id'].'-small" style="background-color: '.$atts['frame_color'].';"><span class="fa fa-'.$icon.'"></span></div> 
			<div class="clearfix"></div>
			<div class="shortmas-flat-caption animated-caption">'.$atts['caption'].'<div class="clearfix"></div></div>
			</div>
			<div class="shortmas-flat-lightbox" id="'.$atts['id'].'-big">
			'.do_shortcode($lb).'
			<div class="sm-flat-close"></div>
			</div>

			<script type="text/javascript">
jQuery(\'#'.$atts['id'].'-small\').click(function(e) {
	jQuery(\'#'.$atts['id'].'-big\').lightbox_me({
		centered: true,
			closeSelector: \'.sm-flat-close\',
			closeClick: true
	});
	e.preventDefault();    
	});
</script>';



return $return;
		}  

/**
 * flatbutton
 */
	public static function flatbutton($atts = null, $content = null)
		{
		$atts=shortcode_atts(array
			(
			'id'           => 'flat-button-id',
			'size'         => 'medium',
			'icon'         => 'icon: search-plus',
			'icon_color'   => '#E9573F',
			'icon_bgcolor' => '#434A54',
			'animation'    => 'yes',
			'align'        => 'none',
			'bgcolor'      => '#E9573F',
			'color'        => '#ffffff',
			'url'          => '#',
			'target'       => '_blank',
			), $atts, 'flatbutton');

		 wp_enqueue_style('font-awesome');               
		// Prepare icon
		if ( $atts['icon'] ) {
			if ( strpos( $atts['icon'], 'icon:' ) !== false ) {
				$icon = trim( str_replace( 'icon:', '', $atts['icon'] ) );
			}
			else $icon = '<img src="' . $atts['icon'] . '" alt="' . esc_attr( $content ) . '" style="' . implode( $img_css, ';' ) . '" />';
		}
		else {  $icon = ''; $style = 'style="display: block;"'; }

		if ( $atts['align'] == 'center' ) { $wrap_start = '<div class="smmb-align-center-wrapper">'; $wrap_end = '</div>'; } else { $wrap_start  = ''; $wrap_end = ''; }

		$return = $wrap_start . '<a id="' . $atts['id'] . '" href="' . $atts['url'] . '" target="' . $atts['target'] . '" class="smmb-wrap smmb-' . $atts['size'] . ' smmb-align-' . $atts['align'] . '" style="background-color: ' . $atts['bgcolor'] . '; color: ' . $atts['color'] . ';">
<span class="smmb-icon smmb-animation-' . $atts['animation'] . '" style="background-color: ' . $atts['icon_bgcolor'] . ';"><i class="fa fa-' . $icon . '" style="color: ' . $atts['icon_color'] . ';"></i></span>
<span class="smmb-text">';
	$return .= $content;
	$return .= '</span></a>' . $wrap_end;



	// Return shortcode string
	return $return;

return $return;
		}                          


	}
