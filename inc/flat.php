<?php
class Shortcodes_Master_Flat {

	/**
	 * Constructor
	 */
	function __construct() {
		// Load textdomain
		load_plugin_textdomain( 'smflat', false, dirname( plugin_basename( SMFLAT_PLUGIN_FILE ) ) . '/lang/' );
		// Reset cache on activation
		if ( class_exists( 'Sm_Generator' ) ) register_activation_hook( SMFLAT_PLUGIN_FILE, array( 'Sm_Generator', 'reset' ) );
		// Init plugin
		add_action( 'plugins_loaded', array( __CLASS__, 'init' ), 20 );
		// Make pluin meta translation-ready
		__( 'ShortcodesMaster', 'smflat' );
		__( 'Shortcodes Master - Flat', 'smflat' );
		__( 'Flat elements addon for Shortcodes Master.', 'smflat' );
	}

	/**
	 * Plugin init
	 */
	public static function init() {
		// Check for SM
		if ( !function_exists( 'shortcodes_master' ) ) {
			// Show notice
			add_action( 'admin_notices', array( __CLASS__, 'sm_notice' ) );
			// Break init
			return;
		}
		// Register assets
		add_action( 'init', array( __CLASS__, 'assets' ) );
		// Add new group to Generator
		add_filter( 'sm/data/groups', array( __CLASS__, 'group' ) );
		// Register new shortcodes
		add_filter( 'sm/data/shortcodes', array( __CLASS__, 'data' ) );
		// Add plugin meta links
		add_filter( 'plugin_row_meta', array( __CLASS__, 'meta_links' ), 10, 2 );
	}

	/**
	 * Install SM notice
	 */
	public static function sm_notice() {
		?><div class="updated">
			<p><?php _e( 'Please install and activate latest version of <b>Shortcodes Master</b> to use it\'s addon <b>Shortcodes Master - Flat</b>.<br />Deactivate this addon to hide this message.', 'smflat' ); ?></p>
			<p><a href="https://lizatom.com/wordpress-shortcodes-plugin/" target="_blank" class="button button-primary"><?php _e( 'Install Shortcodes Master', 'smflat' ); ?> &rarr;</a></p>	
		</div><?php
	}

	public static function assets() {
        $plugins_url = plugins_url();
        wp_register_style( 'jquery-ui', plugins_url( 'assets/css/jquery-ui.css', SMFLAT_PLUGIN_FILE ), false, SMFLAT_PLUGIN_VERSION, 'all' );
		wp_register_style( 'sm-flat', plugins_url( 'assets/css/flat.css', SMFLAT_PLUGIN_FILE ), false, SMFLAT_PLUGIN_VERSION, 'all' );
        wp_register_style( 'font-raleway', 'https://fonts.googleapis.com/css?family=Raleway:400,300', false, SMFLAT_PLUGIN_VERSION, 'all' );
        
        wp_enqueue_style( 'font-awesome' );
        // wp_enqueue_style( 'jquery-ui' );
        wp_enqueue_style( 'sm-flat' );
        wp_enqueue_style( 'font-raleway' );

        wp_enqueue_script( 'jquery' );
        
        wp_register_script('jquery', includes_url('js/jquery/jquery.js', array(), false, false));        
        wp_register_script('jquery-ui-core', includes_url('js/jquery/ui/jquery.ui.core.min.js'), array('jquery'));
        wp_register_script('jquery-ui-draggable', includes_url('js/jquery/ui/jquery.ui.draggable.min.js'), array('jquery'));
        wp_register_script('jquery-ui-droppable', includes_url('js/jquery/ui/jquery.ui.droppable.min.js'), array('jquery'));
        wp_register_script('jquery-ui-dialog', includes_url('js/jquery/ui/jquery.ui.dialog.min.js'), array('jquery'));
        wp_register_script('jquery-ui-widget', includes_url('js/jquery/ui/jquery.ui.widget.min.js'), array('jquery'));
        wp_register_script('jquery-ui-tabs', includes_url('js/jquery/ui/jquery.ui.tabs.min.js'), array('jquery'));
        wp_register_script('jquery-ui-accordion', includes_url('js/jquery/ui/jquery.ui.accordion.min.js'), array('jquery'));
        
        wp_register_script('jquery-effects-core', includes_url('js/jquery/ui/jquery.ui.effect.min.js'), array('jquery'));
        wp_register_script('jquery-effects-blind', includes_url('js/jquery/ui/jquery.ui.effect-blind.min.js'), array('jquery'));
        wp_register_script('jquery-effects-bounce', includes_url('js/jquery/ui/jquery.ui.effect-bounce.min.js'), array('jquery'));
        wp_register_script('jquery-effects-clip', includes_url('js/jquery/ui/jquery.ui.effect-clip.min.js'), array('jquery'));
        wp_register_script('jquery-effects-drop', includes_url('js/jquery/ui/jquery.ui.effect-drop.min.js'), array('jquery'));
        wp_register_script('jquery-effects-explode', includes_url('js/jquery/ui/jquery.ui.effect-explode.min.js'), array('jquery'));
        wp_register_script('jquery-effects-fade', includes_url('js/jquery/ui/jquery.ui.effect-fade.min.js'), array('jquery'));
        wp_register_script('jquery-effects-fold', includes_url('js/jquery/ui/jquery.ui.effect-fold.min.js'), array('jquery'));
        wp_register_script('jquery-effects-highlight', includes_url('js/jquery/ui/jquery.ui.effect-highlight.min.js'), array('jquery'));
        wp_register_script('jquery-effects-pulsate', includes_url('js/jquery/ui/jquery.ui.effect-pulsate.min.js'), array('jquery'));
        wp_register_script('jquery-effects-scale', includes_url('js/jquery/ui/jquery.ui.effect-scale.min.js'), array('jquery'));
        wp_register_script('jquery-effects-shake', includes_url('js/jquery/ui/jquery.ui.effect-shake.min.js'), array('jquery'));
        wp_register_script('jquery-effects-slide', includes_url('js/jquery/ui/jquery.ui.effect-slide.min.js'), array('jquery'));
        
        wp_register_script( 'sm-flat', plugins_url( 'assets/js/flat.js', SMFLAT_PLUGIN_FILE ), array('jquery'), SMFLAT_PLUGIN_VERSION, true );
		wp_register_script( 'lightboxme', plugins_url( 'assets/js/jquery.lightbox_me.js', SMFLAT_PLUGIN_FILE ), array('jquery'), SMFLAT_PLUGIN_VERSION, true );

        
	}

	public static function meta_links( $links, $file ) {
		if ( $file === plugin_basename( SMFLAT_PLUGIN_FILE ) ) $links[] = '<a href="https://lizatom.com/wiki/shortcodes-master-flat/" target="_blank">' . __( 'Help', 'smflat' ) . '</a>';
		return $links;
	}

	/**
	 * Add new group to the Generator
	 */
	public static function group( $groups ) {
		$groups['flat'] = __( 'Flat', 'smflat' );
		return $groups;
	}

	/**
	 * New shortcodes data
	 */
	public static function data( $shortcodes ) {
        
        /*flattabs*/
        
		$shortcodes['flattabs'] = array(
			'name'     => __( 'Flat tabs', 'smflat' ),
			'type'     => 'wrap',
			'group'    => 'flat box',
            'content' =>  __( "[%prefix_flattab title=\"Title 1\"]Content 1[/%prefix_flattab]\n[%prefix_flattab title=\"Title 2\"]Content 2[/%prefix_flattab]\n[%prefix_flattab title=\"Title 3\"]Content 3[/%prefix_flattab]", 'su' ),             
			'desc'     => __( 'Beautiful flat tabs.', 'smflat' ),
			'note'     => sprintf( __( 'Read more on this shortcode %s here %s', 'sm' ), '<a href="https://lizatom.com/wiki/shortcodes-master-flat/#shortcodes-master-flat-shortcodes-shortcodes-overview-flat-tabs" target="_blank">', '&rarr;</a>' ),
			'icon'     => 'folder',
			'function' => array( 'Shortcodes_Master_Flat_Shortcodes', 'flattabs' ),
	            'atts' => array(
                        'id' => array(
                            'default' => 'unique-id',
                            'name' => __( 'ID', 'smflat' ),
                            'desc' => __( 'Required! Insert unique id of the tabs separated by hyphen. Example: my-unique-id', 'smflat')),
                        'navigation' => array(
                            'type' => 'select',
                            'values' => array(
                                'top-left' => __( 'Top left', 'smflat' ),
                                'bottom-left' => __( 'Bottom left', 'smflat' ),
                                'left' => __( 'Left', 'smflat' )
                            ),
                            'default' => 'top-left',
                            'name' => __( 'Navigation', 'smflat' ),
                            'desc' => __( 'Position of the navigation', 'smflat' )
                        ),            
                        'bg_color_default' => array(
                                        'type'    => 'color',
                                        'default' => '#2B5797',
                                        'name'    => __( 'Background color - Default', 'smflat' ),
                                        'desc'    => __( 'Default background color of the tabs', 'smflat' )
                        ),            
                        'bg_color_current' => array(
                                        'type'    => 'color',
                                        'default' => '#EFF4FF',
                                        'name'    => __( 'Background color - Current', 'smflat' ),
                                        'desc'    => __( 'Current background color of the tabs', 'smflat' )
                        ),        
                        'text_color_default' => array(
                                        'type'    => 'color',
                                        'default' => '#ffffff',
                                        'name'    => __( 'Text color - Default', 'smflat' ),
                                        'desc'    => __( 'Default text color of the tabs', 'smflat' )
                        ),
                        'text_color_current' => array(
                                        'type'    => 'color',
                                        'default' => '#1D1D1D',
                                        'name'    => __( 'Text color - Current', 'smflat' ),
                                        'desc'    => __( 'Current text color of the tabs', 'smflat' )
                        )                
                    )
		);
        
         /*flattab*/
        
        $shortcodes['flattab'] = array(
            'name'     => __( 'Flat tab', 'smflat' ),
            'type'     => 'wrap',
            'content'  => 'Tab content.', 
            'group'    => 'flat box',
			'desc'     => __( 'Beautiful flat tabs.', 'smflat' ),
			'note'     => sprintf( __( 'Read more on this shortcode %s here %s', 'sm' ), '<a href="https://lizatom.com/wiki/shortcodes-master-flat/#shortcodes-master-flat-shortcodes-shortcodes-overview-flat-tabs" target="_blank">', '&rarr;</a>' ),
            'icon'     => 'folder',
            'function' => array( 'Shortcodes_Master_Flat_Shortcodes', 'flattab' ),
                'atts' => array(                        
                        'title' => array(
                            'name'    => __( 'Title', 'smflat' ),
                            'desc'    => __( 'Enter title', 'smflat' ),
                            'default' => 'Title'
                            )
                           )
        );

        
        /*flataccordions*/
        
        $shortcodes['flataccordions'] = array(
            'name'     => __( 'Flat accordions', 'smflat' ),
            'type'     => 'wrap',
            'group'    => 'flat box',
            'content' =>  __( "[%prefix_flataccordion title=\"Title 1\"]Content 1[/%prefix_flataccordion]\n[%prefix_flataccordion title=\"Title 2\"]Content 2[/%prefix_flataccordion]\n[%prefix_flataccordion title=\"Title 3\"]Content 3[/%prefix_flataccordion]", 'smflat' ),
			'desc'     => __( 'Beautiful flat accordions.', 'smflat' ),
			'note'     => sprintf( __( 'Read more on this shortcode %s here %s', 'sm' ), '<a href="https://lizatom.com/wiki/shortcodes-master-flat/#shortcodes-master-flat-shortcodes-shortcodes-overview-flat-accordions" target="_blank">', '&rarr;</a>' ),
            'icon'     => 'align-justify',
            'function' => array( 'Shortcodes_Master_Flat_Shortcodes', 'flataccordions' ),
                    'atts' => array(
                        'id' => array(
                            'default' => 'unique-id',
                            'name' => __( 'ID', 'smm' ),
                            'desc' => __( 'Required! Insert unique id of the accordions set separated by hyphen. Example: my-unique-id', 'smm' )
                        ),
                        'bg_header' => array(
                                        'type'    => 'color',
                                        'default' => '#2B5797',
                                        'name'    => __( 'Header background', 'smm' ),
                                        'desc'    => __( 'Background color of the header', 'smm' )
                        ),            
                        'text_header' => array(
                                        'type'    => 'color',
                                        'default' => '#EFF4FF',
                                        'name'    => __( 'Header text', 'smm' ),
                                        'desc'    => __( 'Color of the header text', 'smm' )
                        ),        
                        'bg_content' => array(
                                        'type'    => 'color',
                                        'default' => '#ffffff',
                                        'name'    => __( 'Content background', 'smm' ),
                                        'desc'    => __( 'Color of the content background', 'smm' )
                        ),
                        'text_content' => array(
                                        'type'    => 'color',
                                        'values'  => array( ),
                                        'default' => '#1D1D1D',
                                        'name'    => __( 'Content text', 'smm' ),
                                        'desc'    => __( 'Color of the content text', 'smm' )
                )
            )
        );
        
        /*flataccordion*/
        
        $shortcodes['flataccordion'] = array(
            'name'     => __( 'Flat accordion', 'smflat' ),
            'type'     => 'wrap',
            'group'    => 'flat box',
            'content' =>  __( "Accordion content.", 'smflat' ),
            'desc'     => __( 'Beautiful flat accordions.', 'smflat' ),
			'note'     => sprintf( __( 'Read more on this shortcode %s here %s', 'sm' ), '<a href="https://lizatom.com/wiki/shortcodes-master-flat/#shortcodes-master-flat-shortcodes-shortcodes-overview-flat-accordions" target="_blank">', '&rarr;</a>' ),
            'icon'     => 'align-justify',
            'function' => array( 'Shortcodes_Master_Flat_Shortcodes', 'flataccordion' ),
                    'atts' => array(
                        'title' => array(
                            'default' => 'Title',
                            'name' => __( 'Title', 'smm' ),
                            'desc' => __( 'Accordion title.', 'smm' )
                        )
            )
        );
        
        /*flatinfobox*/
        
        $shortcodes['flatinfobox'] = array(
            'name'     => __( 'Flat infobox', 'smflat' ),
            'type'     => 'wrap',
            'group'    => 'flat box',
            'content' =>  __( "Infobox content.", 'smflat' ),
			'desc'     => __( 'Beautiful flat infobox.', 'smflat' ),
			'note'     => sprintf( __( 'Read more on this shortcode %s here %s', 'sm' ), '<a href="https://lizatom.com/wiki/shortcodes-master-flat/#shortcodes-master-flat-shortcodes-shortcodes-overview-flat-infobox" target="_blank">', '&rarr;</a>' ),
            'icon'     => 'info-circle',
            'function' => array( 'Shortcodes_Master_Flat_Shortcodes', 'flatinfobox' ),
                    'atts' => array(
                        'id' => array(
                                        'default' => 'unique-id-of-the-box',
                                        'name' => __( 'ID', 'smm' ),
                                        'desc' => __( 'Required! Unique ID of the box.', 'smm' )
                        ),
                        'class' => array(
                                        'default' => '',
                                        'name' => __( 'CSS class', 'smm' ),
                                        'desc' => __( 'CSS class', 'smm' )
                        ),            
                        'title' => array(
                                        'default' => 'Title',
                                        'name' => __( 'Title', 'smm' ),
                                        'desc' => __( 'Title of the box.', 'smm' )
                        ),
                        'type' => array(
                                        'type' => 'select',
                                        'values' => array(
                                                'box'   => __( 'box', 'smm' ),
                                                'bubble'   => __( 'bubble', 'smm' )
                                            ),
                                        'default' => 'box',
                                        'name' => __( 'Type', 'smm' ),
                                        'desc' => __( 'Type of box.', 'smm' )
                        ),
                        'icon' => array(
                                        'type' => 'icon',
                                        'default' => 'icon: heart',
                                        'name' => __( 'Icon', 'smm' ),
                                        'desc' => __( 'You can upload custom icon for this button or pick a built-in icon.', 'smm' )
                        ),
                        'bgcolor' => array(
                                        'type' => 'color',
                                        'values' => array( ),
                                        'default' => '#1E7145',
                                        'name' => __( 'Background color', 'smm' ), 
                                        'desc' => __( 'Background color.', 'smm' )
                        ),
                        'text_color' => array(
                                        'type' => 'color',
                                        'values' => array( ),
                                        'default' => '#ffffff',
                                        'name' => __( 'Color of the text', 'smm' ), 
                                        'desc' => __( 'Color of the text.', 'smm' )
                        ),
                        'icon_color' => array(
                                        'type' => 'color',
                                        'values' => array( ),
                                        'default' => '#ffffff',
                                        'name' => __( 'Color of the icon', 'smm' ), 
                                        'desc' => __( 'Color of the icon.', 'smm' )
                        ),        
                        'close_button' => array(
                                        'type' => 'bool',
                                        'default' => 'yes',
                                        'name' => __( 'Close button', 'smm' ),
                                        'desc' => __( 'Display button (a little cross) that closes the box?', 'smm' )
            ))
        );
        
        /*flatdialog*/
        
        $shortcodes['flatdialog'] = array(
            'name'     => __( 'Flat dialog', 'smflat' ),
            'type'     => 'wrap',
            'group'    => 'flat box',
			'content' =>  __( "Dialog content.", 'smflat' ),
			'note'     => sprintf( __( 'Read more on this shortcode %s here %s', 'sm' ), '<a href="https://lizatom.com/wiki/shortcodes-master-flat/#shortcodes-master-flat-shortcodes-shortcodes-overview-flat-dialog" target="_blank">', '&rarr;</a>' ),
            'desc'     => __( 'Beautiful flat dialog.', 'smflat' ),
            'icon'     => 'info-circle',
            'function' => array( 'Shortcodes_Master_Flat_Shortcodes', 'flatdialog' ),
                    'atts' => array(
                                    // ID
                            'id' => array(
                                'default' => 'dialog-window-id',
                                'name' => __( 'ID', 'smm' ),
                                'desc' => __( 'Dialog window ID', 'smm' )
                            ),
                            // Opener ID
                            'opener_id' => array(
                                'default' => 'window-opener-id',
                                'name' => __( 'Opener ID', 'smm' ),
                                'desc' => __( 'ID of the element that opens dialog window upon clicking.', 'smm' )
                            ),            
                            // Title
                            'title' => array(
                                'default' => '',
                                'name' => __( 'Title', 'smm' ),
                                'desc' => __( 'Title of the dialog window.', 'smm' )
                            ),
                            // Title bar background
                            'titlebar_bgcolor' => array(
                                'type' => 'color',
                                'default' => '#1E7145',
                                'name' => __( 'Title bar background.', 'smm' ), 
                                'desc' => __( 'Title bar background color.', 'smm' )
                            ),
                            // Titlebar text
                            'titlebar_textcolor' => array(
                                'type' => 'color',
                                'default' => '#ffffff',
                                'name' => __( 'Title bar text', 'smm' ), 
                                'desc' => __( 'Title bar text color', 'smm' )
                            ),
                            // Content background
                            'content_bgcolor' => array(
                                'type' => 'color',
                                'default' => '#00A300',
                                'name' => __( 'Content background.', 'smm' ), 
                                'desc' => __( 'Content background color.', 'smm' )
                            ),
                            // Content text
                            'content_textcolor' => array(
                                'type' => 'color',
                                'default' => '#ffffff',
                                'name' => __( 'Content text', 'smm' ), 
                                'desc' => __( 'Content text color', 'smm' )
                            ),
                            // Enable window animation?
                            /*'animation' => array(
                                'type' => 'bool',
                                'default' => 'yes',
                                'name' => __( 'Animation', 'smm' ),
                                'desc' => __( 'Enable window animation?', 'smm' )
                            ),*/
                            // Show effect
                            'showeffect' => array(
                                'type' => 'select',
                                'values' => array(
                                    'blind'   => __( 'blind', 'smm' ),
                                    'bounce'   => __( 'bounce', 'smm' ),
                                    'clip'  => __( 'clip', 'smm' ),
                                    'drop' => __( 'drop', 'smm' ),
                                    'explode' => __( 'explode', 'smm' ),
                                    'fade' => __( 'fade', 'smm' ),
                                    'fold' => __( 'fold', 'smm' ),
                                    'highlight' => __( 'highlight', 'smm' ),
                                    'pulsate' => __( 'pulsate', 'smm' ),
                                    'scale' => __( 'scale', 'smm' ),
                                    'shake' => __( 'shake', 'smm' ),
                                    'slide' => __( 'slide', 'smm' )
                                ),
                                'default' => 'blind',
                                'name' => __( 'Show effect', 'smm' ),
                                'desc' => __( 'Animation effect.', 'smm' )
                            ),                
                            // Hide effect
                            'hideeffect' => array(
                                'type' => 'select',
                                'values' => array(
                                    'blind'   => __( 'blind', 'smm' ),
                                    'bounce'   => __( 'bounce', 'smm' ),
                                    'clip'  => __( 'clip', 'smm' ),
                                    'drop' => __( 'drop', 'smm' ),
                                    'explode' => __( 'explode', 'smm' ),
                                    'fade' => __( 'fade', 'smm' ),
                                    'fold' => __( 'fold', 'smm' ),
                                    'highlight' => __( 'highlight', 'smm' ),
                                    'pulsate' => __( 'pulsate', 'smm' ),
                                    'scale' => __( 'scale', 'smm' ),
                                    'shake' => __( 'shake', 'smm' ),
                                    'slide' => __( 'slide', 'smm' )
                                ),
                                'default' => 'explode',
                                'name' => __( 'Hide effect', 'smm' ),
                                'desc' => __( 'Animation effect.', 'smm' )
                            )
            )
        );
        
        
        /*flatframe*/
        
        $shortcodes['flatframe'] = array(
            'name'     => __( 'Flat frame', 'smflat' ),
            'type'     => 'wrap',
            'group'    => 'flat box',
            'content' =>  __( "Lightbox HTML. Leave empty if you are ok with the image you've chosen above or enter iframe code like: <iframe width=\"420\" height=\"315\" src=\"//www.youtube.com/embed/0aA5NxTujqg\" frameborder=\"0\" allowfullscreen></iframe>", 'smflat' ),
            'desc' => __( 'Frame for images with lightbox effect.', 'smflat' ),
            'note'     => sprintf( __( 'Read more on this shortcode %s here %s', 'sm' ), '<a href="https://lizatom.com/wiki/shortcodes-master-flat/#shortcodes-master-flat-shortcodes-shortcodes-overview-flat-frame" target="_blank">', '&rarr;</a>' ),
            'icon'     => 'picture-o',
            'function' => array( 'Shortcodes_Master_Flat_Shortcodes', 'flatframe' ),
                    'atts' => array(
                        'id' => array(
                            'default' => __( 'flat-frame-id', 'smflat' ),
                            'name' => __( 'Unique ID.', 'smflat' ), 
                            'desc' => __( 'This field is required..', 'smflat' )),          
                        'image' => array(
                            'type' => 'upload',
                            'default' => '',
                            'name' => __( 'Image URL', 'smflat' ),
                            'desc' => __( 'Insert URL of the image', 'smflat' )
                        ),
                        'frame_color' => array(
                                'type' => 'color',
                                'default' => '#4A89DC',
                                'name' => __( 'Color of the frame.', 'smflat' ), 
                                'desc' => __( 'Color of the frame and caption.', 'smflat' )
                            ),
                        'icon' => array(
                                        'type' => 'icon',
                                        'default' => 'icon: search-plus',
                                        'name' => __( 'Icon', 'smm' ),
                                        'desc' => __( 'Choose icon.', 'smm' )),    
                        'caption' => array(
                            'default' => __( 'Image caption.', 'smflat' ),
                            'name' => __( 'Image caption.', 'smflat' ), 
                            'desc' => __( 'Enter description of the image.', 'smflat' )    
            )
            )
        );
        
        /*flatbutton*/
        
        $shortcodes['flatbutton'] = array(
            'name'     => __( 'Flat button', 'smflat' ),
            'type'     => 'wrap',
            'group'    => 'flat content',
            'content' =>  __( "Click me!", 'smflat' ),
			'desc' => __( 'Beautiful flat button.', 'smflat' ),
			'note'     => sprintf( __( 'Read more on this shortcode %s here %s', 'sm' ), '<a href="https://lizatom.com/wiki/shortcodes-master-flat/#shortcodes-master-flat-shortcodes-shortcodes-overview-flat-button" target="_blank">', '&rarr;</a>' ),
            'icon'     => 'stop',
            'function' => array( 'Shortcodes_Master_Flat_Shortcodes', 'flatbutton' ),
                    'atts' => array(
                        'id' => array(
                            'default' => __( 'flat-button-id', 'smflat' ),
                            'name' => __( 'Unique ID.', 'smflat' ), 
                            'desc' => __( 'This field is required..', 'smflat' )),          
                        // Size
                        'size' => array(
                            'type' => 'select',
                            'values' => array(
                                'small'  => __( 'small', 'smm' ),
                                'medium' => __( 'medium', 'smm' ),
                                'big'    => __( 'big', 'smm' ),
                                'giant'  => __( 'giant', 'smm' )
                            ),
                            'default' => 'medium',
                            'name'    => __( 'Size', 'smm' ),
                            'desc'    => __( 'Size of button.', 'smm' )
                        ),
                        // icon
                        'icon' => array(
                                        'type' => 'icon',
                                        'default' => 'icon: search-plus',
                                        'name' => __( 'Icon', 'smm' ),
                                        'desc' => __( 'Choose icon.', 'smm' )),
                        // icon color
                        'icon_color' => array(
                                'type' => 'color',
                                'default' => '#ffffff',
                                'name' => __( 'Color of the icon.', 'smflat' ), 
                                'desc' => __( 'Color of the icon.', 'smflat' )
                            ),
                         // Animation
                         'animation' => array(
                        'type' => 'bool',
                        'default' => 'yes',
                        'name' => __( 'Animation', 'smm' ), 
                        'desc' => __( 'Do you want to animate on hover?', 'sm' )
                        ),
                        // Align
                        'align' => array(
                            'type' => 'select',
                            'values' => array(
                                    'none'   => __( 'none', 'smm' ),
                                    'left'   => __( 'left', 'smm' ),
                                    'right'  => __( 'right', 'smm' ),
                                    'center' => __( 'center', 'smm' )
                            ),
                            'default' => 'none',
                            'name' => __( 'Align', 'smm' ),
                            'desc' => __( 'Align of button.', 'smm' )
                        ),
                        // bgcolor
                        'bgcolor' => array(
                                'type' => 'color',
                                'default' => '#E9573F',
                                'name' => __( 'Background color', 'smflat' ), 
                                'desc' => __( 'Background color.', 'smflat' )
                            ),
                        // icon_bgcolor
                        'icon_bgcolor' => array(
                                'type' => 'color',
                                'default' => '#434A54',
                                'name' => __( 'Icon background color', 'smflat' ), 
                                'desc' => __( 'Icon background color.', 'smflat' )
                            ),
                        // color
                        'color' => array(
                                'type' => 'color',
                                'default' => '#ffffff',
                                'name' => __( 'Text color', 'smflat' ), 
                                'desc' => __( 'Text color.', 'smflat' )
                            ),
                        // icon_color
                        'icon_color' => array(
                                'type' => 'color',
                                'default' => '#E9573F',
                                'name' => __( 'Icon color', 'smflat' ), 
                                'desc' => __( 'Icon color.', 'smflat' )
                            ),    
                       // URL
                       'url' => array(
                            'default' => 'http://mysite.com',
                            'name' => __( 'URL', 'smflat' ),
                            'desc' => __( 'URL of button', 'smflat' )
                        ),
                       // Target
                       'target' => array(
                            'type' => 'select',
                            'values' => array(
                                '_top'       => __( 'This tab', 'smm' ),
                                '_blank'  => __( 'New tab', 'smm' )
                            ),
                            'default' => '_blank',
                            'name' => __( 'Target', 'smm' ),
                            'desc' => __( 'Where do you want to open the URL?', 'smm' )
                        )                 
                        
            )
        );        
        
		return $shortcodes;
	}
}

new Shortcodes_Master_Flat;
