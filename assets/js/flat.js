jQuery(document).ready(function() {

/* Close boxes
/* =============================================== */
	jQuery('.shortmas-close-box').click(function(){
		jQuery(this).parent().fadeOut('slow');
	});

/* Caption
/* =============================================== */
	jQuery('.shortmas-flat-image-frame .shortmas-flat-box-image').each(function(){
            var imgWidth = jQuery(this).width();
		    var caption = jQuery(this).next().next();
            jQuery(this).parent().css({'width': imgWidth+20, 'padding-left': 10, 'padding-right': 10, 'padding-top': 10, 'padding-bottom': 10 })
	});

/* Lists
/* =============================================== */
	jQuery('.shortmas-flat-list li').each(function(){
		var listIcon = jQuery(this).parent().attr('data-flat-icon');
		jQuery(this).prepend('<span  class="'+listIcon+'"></span> ');
	});

/* Tabs
/* =============================================== */

	jQuery('.shortmas-flat-tabs').each(function(){
		var tabsId = jQuery(this).attr('id');
		jQuery('#' + tabsId).tabs();
	});
	/*jQuery('.shortmas-flat-tabs.tabs-bottom').each(function(){
		jQuery(this).children().filter('.ui-tabs-nav').appendTo( ".tabs-bottom" );
	});*/

	jQuery('.shortmas-flat-tabs ul.ui-tabs-nav li').each(function(){
		var tabDefaultBg = jQuery(this).parent().parent().attr('data-flat-tab-default-bg');
		var tabDefaultText = jQuery(this).parent().parent().attr('data-flat-tab-default-text');
		var tabCurrentBg = jQuery(this).parent().parent().attr('data-flat-tab-current-bg');
		var tabCurrentText = jQuery(this).parent().parent().attr('data-flat-tab-current-text');
				
		jQuery(this).css({'background':tabCurrentBg, 'color': tabCurrentText}).children().css({'color': tabCurrentText}); // tabs navigation current
		jQuery(this).parent().parent().find('div').css({'background':tabCurrentBg, 'color': tabCurrentText}); // tabs content current
		jQuery(this).parent().find('li').css({'background': tabDefaultBg, 'color': tabDefaultText }).children().css({'color': tabDefaultText}); // tabs navigation default
		jQuery(this).parent().find("li.ui-state-active.ui-tabs-active").css({"background": tabCurrentBg, "color": tabCurrentText }).children().css({"color": tabCurrentText}); // tabs navigation active
	});

	jQuery('.shortmas-flat-tabs ul.ui-tabs-nav li').click(function(){
		var tabDefaultBg = jQuery(this).parent().parent().attr('data-flat-tab-default-bg');
		var tabDefaultText = jQuery(this).parent().parent().attr('data-flat-tab-default-text');
		var tabCurrentBg = jQuery(this).parent().parent().attr('data-flat-tab-current-bg');
		var tabCurrentText = jQuery(this).parent().parent().attr('data-flat-tab-current-text');
		
		jQuery(this).parent().find('li').css({'background': tabDefaultBg, 'color': tabDefaultText }).children().css({'color': tabDefaultText}); // tabs navigation default
		jQuery(this).css({'background':tabCurrentBg, 'color': tabCurrentText}).children().css({'color': tabCurrentText}); // tabs nvigation current
		jQuery(this).parent().parent().find('div').css({'background':tabCurrentBg, 'color': tabCurrentText}); // tabs content
	});

/* Accordions
/* =============================================== */
	jQuery('.shortmas-flat-accordion').each(function(){
		var accordionId = jQuery(this).attr('id');
		jQuery('#' + accordionId).accordion();
	});

	jQuery('.shortmas-flat-accordion').each(function(){
		var accHeaderBg = jQuery(this).attr('data-flat-acc-header-bg');
		var accHeaderText = jQuery(this).attr('data-flat-acc-header-text');
		var accContentBg = jQuery(this).attr('data-flat-acc-content-bg');
		var accContentText = jQuery(this).attr('data-flat-acc-content-text');

		jQuery(this).find('.ui-accordion-header').css({'background': accHeaderBg, 'color': accHeaderText});
		jQuery(this).find('.ui-accordion-content').css({'background': accContentBg, 'color': accContentText});
	});

/* Frame lightbox
/* =============================================== */    
    jQuery('.shortmas-flat-lightbox').hide();
    
});
